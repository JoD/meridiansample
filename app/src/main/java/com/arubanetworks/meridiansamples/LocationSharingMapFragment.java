package com.arubanetworks.meridiansamples;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.app.AlertDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.URLUtil;

import com.arubanetworks.meridian.Meridian;
import com.arubanetworks.meridian.editor.EditorKey;
import com.arubanetworks.meridian.internal.util.Dev;
import com.arubanetworks.meridian.internal.util.Strings;
import com.arubanetworks.meridian.location.MeridianLocation;
import com.arubanetworks.meridian.locationsharing.Friend;
import com.arubanetworks.meridian.locationsharing.LocationSharing;
import com.arubanetworks.meridian.locationsharing.LocationSharingException;
import com.arubanetworks.meridian.locationsharing.User;
import com.arubanetworks.meridian.maps.MapFragment;
import com.arubanetworks.meridian.maps.MapOptions;
import com.arubanetworks.meridian.maps.MapView;
import com.arubanetworks.meridian.maps.Marker;
import com.arubanetworks.meridian.maps.PlacemarkMarker;
import com.arubanetworks.meridian.maps.Transaction;
import com.arubanetworks.meridian.maps.directions.DirectionsDestination;
import com.arubanetworks.meridian.maps.directions.DirectionsSource;
import com.arubanetworks.meridiansamples.utils.CropCircleTransformation;
import com.arubanetworks.meridiansamples.utils.Lists;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class LocationSharingMapFragment extends MapFragment implements MapView.DirectionsEventListener, MapView.MapEventListener {
    private final User sampleUser = new User();
    /**
     * Friends
     **/
    private List<Friend> friendsList = new ArrayList<>();

    Map<String, Marker> friendMarkers = new HashMap<>();
    Timer friendsTimer;
    TimerTask friendsTimerTask;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.e("locationsharing","location sharing ONCREATE");

        // we need to initialize Location Sharing first
        if (!Strings.isNullOrEmpty(Meridian.getShared().getEditorToken())) {
            LocationSharing.initWithAppKey(Application.APP_KEY);
        }else {
            Log.e("locationsharing","location sharing not initialized");
        }

        CallbackCollector<Object> cbc = new CallbackCollector<Object>(1, () -> {
            LocationSharing.shared().acceptInvite("6304867850649600",new LocationSharing.Callback<Friend>(){
                @Override
                public void onSuccess(Friend f) {
                    Log.e("locationsharing","Friend " + f.getFullName() + " SUCCESS");
                    onResume();
                }
                @Override
                public void onError(LocationSharingException e) {
                    Log.e("locationsharing","Friend FAILED");
                }
            });
            return new Object();
        });

        sampleUser.setFullName("Freddy");
        LocationSharing.shared().createUser(sampleUser, new LocationSharing.Callback<User>() {
            @Override
            public void onSuccess(User u) {
                Log.e("locationsharing","User " +u.getFullName() +" created SUCCESS");
                Log.e("locationsharing","Credentials " +u.getCredentials());
                LocationSharing.shared().startPostingLocationUpdates(getActivity().getApplicationContext());
                cbc.call();
            }

            @Override
            public void onError(LocationSharingException t) {
                Log.e("locationsharing","User creation FAILED");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.e("locationsharing","Resuming...");
        if (LocationSharing.shared().isUploadingServiceRunning()) {
            friendsTimer = new Timer();
            friendsTimerTask = new TimerTask() {
                @Override
                public void run() {
                    LocationSharing.shared().getFriends(new LocationSharing.Callback<List<Friend>>() {
                        @Override
                        public void onSuccess(List<Friend> result) {
                            renderFriends(result);
                        }

                        @Override
                        public void onError(LocationSharingException lse) {
                            Log.e("locationsharing","Failed to retrieve friends to show on map for user " + LocationSharing.shared().getCurrentUser().getFullName());
                        }
                    });
                }
            };
            friendsTimer.schedule(friendsTimerTask, new Date(), 5000);
        }else{
            Log.e("locationsharing","uploading service is not running");
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (friendsTimer != null) {
            friendsTimer.cancel();
            friendsTimer = null;
        }

        if (friendsTimerTask != null) {
            friendsTimerTask.cancel();
            friendsTimerTask = null;
        }
    }

    private class LocationSharingMarker extends Marker {

        private static final int SIZE_AVATAR_DP = 40;

        private Friend friend;
        private Bitmap bitmap;

        public LocationSharingMarker(Context context, Friend friend) {
            super(friend.getLocation().getX(), friend.getLocation().getY());
            this.friend = friend;
            setWeight(1.1f);
            setName(friend.getFullName());
        }

        public Friend getFriend() {
            return friend;
        }

        @Override
        public boolean getCollision() {
            return false;
        }

        @Override
        public Bitmap getBitmap() {
            if (bitmap != null) {
                return bitmap;
            }

            if (URLUtil.isValidUrl(friend.getPhotoUrl())) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.with(getActivity().getApplicationContext())
                                .load(friend.getPhotoUrl())
                                .asBitmap()
                                .centerCrop()
                                .transform(new CropCircleTransformation(Glide.get(getActivity().getApplicationContext()).getBitmapPool(), Dev.get().dpToPix(2)))
                                .into(new SimpleTarget<Bitmap>(Math.round(Dev.get().dpToPix(SIZE_AVATAR_DP)), Math.round(Dev.get().dpToPix(SIZE_AVATAR_DP))) {
                                    @Override
                                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                                        LocationSharingMarker.this.bitmap = bitmap;
                                        LocationSharingMarker.this.invalidate(true);
                                    }
                                });
                    }
                });
            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap initialsBitmap = friend.getInitialsBitmap(Dev.get().dpToPix(SIZE_AVATAR_DP), Dev.get().dpToPix(SIZE_AVATAR_DP), Dev.get().dpToPix(24), getResources().getColor(R.color.mr_callout_blue));

                        // convert to byte array
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        initialsBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

                        // draw
                        Glide.with(getActivity().getApplicationContext())
                                .fromBytes()
                                .load(stream.toByteArray())
                                .bitmapTransform(new CropCircleTransformation(Glide.get(getActivity().getApplicationContext()).getBitmapPool(), Dev.get().dpToPix(2)))
                                .into(new SimpleTarget<GlideDrawable>(Dev.get().dpToPix(SIZE_AVATAR_DP), Dev.get().dpToPix(SIZE_AVATAR_DP)) {
                                    @Override
                                    public void onResourceReady(GlideDrawable drawable, GlideAnimation anim) {
                                        LocationSharingMarker.this.bitmap = ((GlideBitmapDrawable) drawable).getBitmap();
                                        LocationSharingMarker.this.invalidate(true);
                                    }
                                });
                    }
                });
            }

            // still loading
            return null;

        }
    }

    private void renderFriends(final List<Friend> friends) {
        Log.e("locationsharing","rendering friends :)");
        MapView mapView = getMapView();

        if (mapView == null || mapView.getMapKey() == null || friends == null) {
            if (friendMarkers != null){
                friendMarkers.clear();
            }
        }

        // remove friends who are gone or in another map
        List<Friend> friendsToRemove = Lists.filter(friendsList, new Lists.IPredicate<Friend>() {
            @Override
            public boolean apply(Friend friend) {
                return !friends.contains(friend) || !friend.isInSameMapAndSharing(mapView.getMapKey());
            }
        });
        List<Marker> markersToRemove = new ArrayList<>();
        for (Friend friend : friendsToRemove) {
            if (friendMarkers.get(friend.getKey()) != null) {
                markersToRemove.add(friendMarkers.get(friend.getKey()));
                friendMarkers.remove(friend.getKey());
            }
        }
        if (!markersToRemove.isEmpty()) {
            mapView.commitTransaction(new Transaction.Builder().setType(Transaction.Type.REMOVE).setAnimationDuration(0).addMarkers(markersToRemove).build());
        }
        friends.removeAll(friendsToRemove);
        Log.e("locationsharing","still rendering friends?? " + friends.size() + " friends.");

        // add the new friends
        List<Friend> friendsToAdd = Lists.filter(friends, new Lists.IPredicate<Friend>() {
            @Override
            public boolean apply(Friend friend) {
                return friendMarkers.get(friend.getKey()) == null && friend.isInSameMapAndSharing(mapView.getMapKey());
            }
        });
        for (Friend friend : friendsToAdd) {
            if (friend.getLocation().getMapKey().equals(mapView.getMapKey())) {
                friendMarkers.put(friend.getKey(), new LocationSharingMarker(getActivity(), friend));
            }
        }
        Log.e("locationsharing","Updating before with " + friends.size() + " friends.");
        // update the existing friends with the location
        List<Friend> friendsToUpdate = Lists.filter(friends, new Lists.IPredicate<Friend>() {
            @Override
            public boolean apply(Friend friend) {
                return friendsList.contains(friend) && friend.isInSameMapAndSharing(mapView.getMapKey());
            }
        });
        Log.e("locationsharing","Updating " + friendsToUpdate.size() + " friends.");
        for (Friend friend : friendsToUpdate) {
            Log.e("locationsharing","Updating " + friend.getFullName());
            friendMarkers.get(friend.getKey()).setPosition(friend.getLocation().getX(), friend.getLocation().getY());
        }
        if (!friendMarkers.isEmpty()) {
            mapView.commitTransaction(new Transaction.Builder().setAnimationDuration(250).addMarkers(friendMarkers.values()).build());
        }

        friendsList = friends;
    }

    //
    // MapViewListener methods
    //
    @Override
    public void onMapLoadStart() {
        super.onMapLoadStart();

        Log.e("locationsharing","Loading map.");

        // time to remove our markers
        if (friendMarkers != null) friendMarkers.clear();
    }

    @Override
    public void onMapLoadFinish() {
        super.onMapLoadFinish();
    }

    @Override
    public void onPlacemarksLoadFinish() {
        super.onPlacemarksLoadFinish();
    }

    @Override
    public void onMapRenderFinish() {
        super.onMapRenderFinish();
    }

    @Override
    public void onMapLoadFail(Throwable tr) {
        super.onMapLoadFail(tr);
    }

    @Override
    public void onMapTransformChange(Matrix transform) {
        super.onMapTransformChange(transform);
    }

    @Override
    public void onDirectionsReroute() {
        super.onDirectionsReroute();
    }

    @Override
    public boolean onDirectionsClick(Marker marker) {
        if (marker instanceof PlacemarkMarker) {
            return super.onDirectionsClick(marker);

        } else if (marker instanceof LocationSharingMarker) {
            Friend friend = ((LocationSharingMarker) marker).getFriend();
            if (friend != null && friend.getLocation() != null) {
                startDirections(DirectionsDestination.forMapPoint(friend.getLocation().getMapKey(),
                        new PointF(friend.getLocation().getX(), friend.getLocation().getY())));
            }
        }
        return false;
    }

    @Override
    public boolean onDirectionsStart() {
        return super.onDirectionsStart();
    }

    @Override
    public boolean onRouteStepIndexChange(int index) {
        return super.onRouteStepIndexChange(index);
    }

    @Override
    public boolean onDirectionsClosed() {
        return super.onDirectionsClosed();
    }

    @Override
    public boolean onDirectionsError(Throwable tr) {
        return super.onDirectionsError(tr);
    }

    @Override
    public void onLocationUpdated(MeridianLocation meridianLocation) {
        super.onLocationUpdated(meridianLocation);
    }

    @Override
    public void onUseAccessiblePathsChange() {
        super.onUseAccessiblePathsChange();
    }

    public static class Builder {
        MapOptions a;
        EditorKey b;
        EditorKey c;
        String d;
        DirectionsSource e;
        DirectionsDestination f;

        public Builder() {
        }

        public LocationSharingMapFragment.Builder setMapOptions(MapOptions mapOptions) {
            this.a = mapOptions;
            return this;
        }

        public LocationSharingMapFragment.Builder setAppKey(EditorKey appKey) {
            this.b = appKey;
            return this;
        }

        public LocationSharingMapFragment.Builder setMapKey(EditorKey mapKey) {
            this.c = mapKey;
            return this;
        }

        public LocationSharingMapFragment.Builder setPlacemarkId(String placemarkId) {
            this.d = placemarkId;
            return this;
        }

        public LocationSharingMapFragment.Builder setSource(DirectionsSource source) {
            this.e = source;
            return this;
        }

        public LocationSharingMapFragment.Builder setDestination(DirectionsDestination destination) {
            this.f = destination;
            return this;
        }

        public LocationSharingMapFragment build() {
            if(this.c == null && this.b == null) {
                throw new IllegalStateException("You need to provide a mapKey or appKey");
            } else {
                LocationSharingMapFragment var1 = new LocationSharingMapFragment();
                Bundle var2 = var1.getArguments();
                if(var2 == null) {
                    var2 = new Bundle();
                }

                if(this.a == null) {
                    this.a = MapOptions.getDefaultOptions();
                }

                var2.putSerializable("meridian.mapOptions", this.a);
                if(this.c != null) {
                    var2.putSerializable("meridian.MapKey", this.c);
                    var2.putSerializable("meridian.AppKey", this.c.getParent());
                } else if(this.b != null) {
                    var2.putSerializable("meridian.AppKey", this.b);
                }

                if(!Strings.isNullOrEmpty(this.d)) {
                    var2.putString("meridian.PlacemarkKey", this.d);
                }

                if(this.e != null && this.f == null) {
                    throw new IllegalArgumentException("destination must not be null.");
                } else {
                    if(this.e != null) {
                        var2.putSerializable("meridian.FromKey", this.e);
                    }

                    if(this.f != null) {
                        var2.putSerializable("meridian.PendingDestinationKey", this.f);
                    }

                    var1.setArguments(var2);
                    return var1;
                }
            }
        }
    }

    private static final int IMAGE_REQUEST_FOR_USER_IMAGE = 0x0000000F;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode != Activity.RESULT_OK)return;
        switch (requestCode){
            case IMAGE_REQUEST_FOR_USER_IMAGE:
                uploadURIasUserImage(data.getData());
                break;
        }
    }

    private void uploadURIasUserImage(Uri uri){
        if(uri == null || getContext() == null)return;
        LocationSharing.shared().uploadUserPhoto(getContext(), uri, new LocationSharing.Callback<User>() {
            @Override
            public void onSuccess(User result) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Success! image uploaded.")
                        .setPositiveButton("OK", null)
                        .show();
            }

            @Override
            public void onError(LocationSharingException t) {
                new AlertDialog.Builder(getActivity())
                        .setMessage(String.format("Error uploading image. %s", t.getErrorMessage()))
                        .setPositiveButton("OK", null)
                        .show();
            }
        });
    }

}
