package com.arubanetworks.meridiansamples;

import java.util.function.Supplier;

public class CallbackCollector<T> {
    private int numberCalled = 0;
    public final int numberToCall;
    private final Supplier<T> func;

    CallbackCollector(int n, Supplier<T> f){
        numberToCall = n;
        func=f;
    }

    public void call(){
        numberCalled++;
        if(numberCalled>=numberToCall){
            func.get();
        }
    }

}
